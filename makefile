all:			geometrica.o triangulo.o quadrado.o main.o
				g++ -o main triangulo.o quadrado.o geometrica.o main.o

geometrica.o:	geometrica.hpp geometrica.cpp
				g++ -c geometrica.cpp

triangulo.o:	triangulo.hpp triangulo.cpp
				g++ -c triangulo.cpp

quadrado.o:		quadrado.hpp quadrado.cpp
				g++ -c quadrado.cpp

main.o:			triangulo.hpp quadrado.hpp main.cpp 
				g++ -c main.cpp

clean:		
				rm -rf *.o

run:		
				./main
