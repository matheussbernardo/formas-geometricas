#include "quadrado.hpp"

Quadrado::Quadrado(){
	setBase(10);
	setAltura(10);
}

Quadrado::Quadrado(float base,float altura){
	setBase(base);
	setAltura(altura);
}

float Quadrado::area(){
	return (getBase()*getAltura());
}

