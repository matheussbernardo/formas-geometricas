#include "triangulo.hpp"
#include "quadrado.hpp"

#include<iostream>

using namespace std;

int main(){

	Triangulo umTriangulo(10,45);
	Quadrado umQuadrado(50,50); 
	cout << "Area do triangulo: " << umTriangulo.area() << endl;
	cout << "Area do quadrado: " << umQuadrado.area() << endl;

	return 0;
}
