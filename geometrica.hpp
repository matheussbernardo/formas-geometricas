#ifndef GEOMETRICA_H
#define GEOMETRICA_H

class Geometrica {
	private:
		float base, altura;
	public:
		Geometrica();
		Geometrica(float base, float altura);
		
		float getBase();
		float getAltura();
		void setBase(float base);
		void setAltura(float altura);
		
		//polimorfismo
		virtual float area() = 0;	
};

#endif
