#include "triangulo.hpp"

Triangulo::Triangulo(){
	setBase(10);
	setAltura(10);
}

Triangulo::Triangulo(float base,float altura){
	setBase(base);
	setAltura(altura);
}

float Triangulo::area(){
	return (getBase()*getAltura())/2;
}
